package com.aws.serviceone.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("service-two")
public interface ServiceTwoClient {
    
    @GetMapping("api/v1/service-two")
    public ResponseEntity<String> whoiam();

    @GetMapping("api/v1/service-two/test")
    public ResponseEntity<String> test();

    @GetMapping("api/v1/service-two/name")
    public ResponseEntity<String> name();
}
