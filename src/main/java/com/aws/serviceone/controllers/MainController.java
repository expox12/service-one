package com.aws.serviceone.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.aws.serviceone.clients.ServiceTwoClient;

@RestController
@RequestMapping("api/v1/service-one")
public class MainController {

    @Autowired
    ServiceTwoClient serviceTwoClient;

    @GetMapping
    public ResponseEntity<String> whoiam() {
        return ResponseEntity.ok("service-one, OK!");
    }

    @GetMapping("/error")
    public ResponseEntity<String> getError() {
        throw new NullPointerException("Nulazo al canto !!");
    }

    @GetMapping("/service-two")
    public ResponseEntity<String> getServiceTwoInfo() {
        return serviceTwoClient.whoiam();
    }

    @GetMapping("/service-two/test")
    public ResponseEntity<String> serviceTwoTest() {
        return serviceTwoClient.test();
    }

    @GetMapping("/service-two/name")
    public ResponseEntity<String> name() {
        return serviceTwoClient.name();
    }
    
}
